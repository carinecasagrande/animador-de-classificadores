package br.unifil.dc.lab2;

import java.util.List;
import java.util.ListIterator;
import java.util.ArrayList;
import java.awt.Color;

/**
 * Write a description of class Gravador here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Gravador {
    public Gravador() {
        this.seqGravacoes = new ArrayList<Transparencia>();
    }

    public ListIterator<Transparencia> getFilme() {
        return seqGravacoes.listIterator();
    }

    private List<Transparencia> seqGravacoes;

    /**
     * Grava a lista, juntamente com uma lista de cores e o nome da mesma
     *
     * @param lista lista a ser gravada
     * @param nome  nome da lista
     */
    public void gravarLista(List<Integer> lista, String nome) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size());
        ListaGravada gravacao = new ListaGravada(copia, cores, nome);
        seqGravacoes.add(gravacao);
    }

    /**
     * Grava a lista, juntamente com uma lista de cores e o nome da mesma, sendo que o indice informado
     * terá a cor amarela
     *
     * @param lista lista a ser gravada
     * @param i     indice que terá a cor difinida como amarelo
     * @param nome  nome da lista
     */
    public void gravarIndiceDestacado(List<Integer> lista, int i, String nome) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size());
        cores.set(i, Color.YELLOW);
        ListaGravada gravacao = new ListaGravada(copia, cores, nome);
        seqGravacoes.add(gravacao);
    }

    /**
     * Grava a lista no momento da comparação de dois indices
     *
     * @param lista lista a ser gravada
     * @param i     indice de um dos valores que foram comparados
     * @param j     indice do outro valor que foi comparado
     */
    public void gravarComparacaoSimples(List<Integer> lista, int i, int j) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size());
        cores.set(i, Color.GRAY);
        cores.set(j, Color.GRAY);
        ListaGravada gravacao = new ListaGravada(copia, cores, "Comparação");
        seqGravacoes.add(gravacao);
    }

    /**
     * Grava a lista no momento da comparação de dois indices, quando feito pelo método Merge Sort
     *
     * @param lista lista a ser gravada
     * @param valor1     valor de um dos indices que foi comparado
     * @param valor2     valor do outro indice que foi comparado
     */
    public void gravarComparacaoMerge(List<Integer> lista, int valor1, int valor2) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size());
        for(int i = 0; i < copia.size(); i++){
            if(lista.get(i) == valor1 || lista.get(i) == valor2){
                cores.set(i, Color.YELLOW);
            }
        }
        ListaGravada gravacao = new ListaGravada(copia, cores, "Comparação MergeSort");
        seqGravacoes.add(gravacao);
    }

    /**
     * Grava a lista pós troca de posição de dois indices
     *
     * @param lista lista a ser gravada
     * @param i     indice de um dos elementos que foram trocados
     * @param j     indice do outro elemento que foi trocado
     */
    public void gravarPosTrocas(List<Integer> lista, int i, int j) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size());
        cores.set(i, Color.YELLOW);
        cores.set(j, Color.YELLOW);
        ListaGravada gravacao = new ListaGravada(copia, cores, "Pós-troca");
        seqGravacoes.add(gravacao);
    }

    /**
     * Grava a lista pós troca de pivo, quando utilizado o método Quicksort
     *
     * @param lista lista a ser gravada
     * @param i     indice de um dos elementos que foram trocados
     * @param j     indice do outro elemento que foi trocado
     */
    public void gravarPosTrocasPivo(List<Integer> lista, int i, int j) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size());
        cores.set(i, Color.RED);
        cores.set(j, Color.RED);
        ListaGravada gravacao = new ListaGravada(copia, cores, "Pós-troca");
        seqGravacoes.add(gravacao);
    }

    /**
     * Gera uma lista de cores com x números de elementos
     *
     * @param numElems quantidade de elementos que a lista deverá ter
     * @return lista de cores
     */
    private static List<Color> novaListaColors(int numElems) {
        ArrayList<Color> lista = new ArrayList<>(numElems);
        for (; numElems > 0; numElems--) lista.add(null);
        return lista;
    }
}
