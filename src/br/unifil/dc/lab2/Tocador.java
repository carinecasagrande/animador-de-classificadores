package br.unifil.dc.lab2;

import javax.swing.JPanel;
import javax.swing.BorderFactory;
import java.awt.*;
import java.util.ListIterator;

/**
 * Write a description of class Tocador here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Tocador extends JPanel {

    public Tocador(ListIterator<Transparencia> quadrosFilme) {
        setBackground(Color.WHITE);
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        carregarFilme(quadrosFilme);
    }
    public Tocador() {
        this(null);
    }
    
    public void carregarFilme(ListIterator<Transparencia> quadrosFilme) {
        this.quadrosFilme = quadrosFilme;
        this.quadroAtual = null;
        numQuadro = 0;
    }
    
    public void avancarFilme() {
        if (quadrosFilme.hasNext()) {
            quadroAtual = quadrosFilme.next();
            numQuadro++;
        }
    }
    
    public void voltarFilme() {
        if (quadrosFilme.hasPrevious()) {
            quadroAtual = quadrosFilme.previous();
            numQuadro--;
        }
    }
    
    public void rebobinarFilme() {
        while (quadrosFilme.hasPrevious()) {
            quadroAtual = quadrosFilme.previous();
            numQuadro--;
        }
    }
    
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D pincel = (Graphics2D) g;

        if (quadroAtual != null) {
            quadroAtual.pintar(pincel, this);
        } else {
            // ESCREVER NO MEIO DA TELA "O Filme ainda não iniciou."
            pincel.setFont(new Font("Arial", Font.BOLD, 30));
            FontMetrics fm = pincel.getFontMetrics();
            String msg = "O Filme ainda não iniciou.";
            int msgWidth = fm.stringWidth(msg);
            int msgAscent = fm.getAscent();
            int msgX = getWidth() / 2 - msgWidth / 2;
            int msgY = getHeight() / 2 + msgAscent / 2;
            pincel.drawString(msg, msgX, msgY);
        }
        
        // ESCREVER NO CANTO INFERIOR DIREITO DA TELA "Quadro 'numQuadro'"
        pincel.setFont(new Font("Arial", Font.ITALIC, 12));
        pincel.drawString("Quadro "+numQuadro, getWidth() - 70, getHeight() - 20);
    }
    
    private int numQuadro = 0;
    private Transparencia quadroAtual = null;
    private ListIterator<Transparencia> quadrosFilme = null;
}
