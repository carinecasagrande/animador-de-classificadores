package br.unifil.dc.lab2;

import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Color;
import java.util.List;

/**
 * @author Carine Casa Grande
 * @version 2020-04-20
 */
public class ListaGravada implements Transparencia
{
    /**
     * Constructor for objects of class ListaGravada
     */
    public ListaGravada(List<Integer> lista, List<Color> coresIndices, String nome) {
        this.lista = lista;
        this.nome = nome;
        this.coresIndices = coresIndices;
    }
    
    public void pintar(Graphics2D pincel, JPanel contexto) {
        Dimension dim = contexto.getSize();
        String valor = "";

        int maiorIdx = encontrarIndiceMaiorElem(lista, 0);
        int altura = 0;
        int x = (80*(dim.width-20))/100;
        int y = (20*(dim.width-20))/100;
        int largura = x / lista.size();
        int distancia = y / lista.size();
        int total = largura + distancia;
        for(int i = 0; i < lista.size(); i++){

                Color myColor = Color.BLUE;
                if(coresIndices.get(i) != null) {
                    myColor = coresIndices.get(i);
                }

                if (i == maiorIdx) altura = 300;
                altura = (lista.get(i) * 300) / lista.get(maiorIdx);

                pincel.setColor(myColor);
                if (i == 0) pincel.fillRect(20, 320 - altura, largura, altura);
                pincel.fillRect(20 + (i * total), 320 - altura, largura, altura);

                pincel.setColor(Color.BLACK);
                if (i == 0) pincel.drawRect(20, 320 - altura, largura, altura);
                pincel.drawRect(20 + (i * total), 320 - altura, largura, altura);

                valor = Integer.toString(lista.get(i));
                pincel.drawString(valor, 20 + (i * total) + (largura / valor.length()), 340);

        }


        /* Implementação da primeira parte: 2.1 Desenhista de listas */
        /*Dimension dim = contexto.getSize();
        String valor = "";
        int maiorIdx = encontrarIndiceMaiorElem(lista, 0);
        int altura = 0;
        int x = (80*(dim.width-20))/100;
        int y = (20*(dim.width-20))/100;
        int largura = x / lista.size();
        int distancia = y / lista.size();
        int total = largura + distancia;
        for(int i = 0; i < lista.size(); i++){
            if(i == maiorIdx) altura = 300;
            altura = (lista.get(i)*300)/lista.get(maiorIdx);

            pincel.setColor(Color.PINK);
            if(i == 0) pincel.fillRect(20,320-altura, largura, altura);
            pincel.fillRect(20+(i*total),320-altura, largura, altura);

            pincel.setColor(Color.BLACK);
            if(i == 0) pincel.drawRect(20,320-altura, largura, altura);
            pincel.drawRect(20+(i*total),320-altura, largura, altura);

            valor = Integer.toString(lista.get(i));
            pincel.drawString(valor, 20+(i*total)+(largura/valor.length()), 340);
        }*/
    }

    private static int encontrarIndiceMaiorElem(List<Integer> lista, int idxInicio) {
        int maior = idxInicio;
        for (int i = idxInicio+1; i < lista.size(); i++) {
            if (lista.get(maior) < lista.get(i)) maior = i;
        }

        return maior;
    }
    
    private List<Integer> lista;
    private List<Color> coresIndices;
    private String nome;
}
