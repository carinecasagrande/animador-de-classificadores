package br.unifil.dc.lab2;

import javax.swing.*;
import java.util.*;

/**
 * Write a description of class AlgoritmosAnimados here.
 * 
 * @author Ricardo Inacio
 * @version 20200408
 */
public class AlgoritmosAnimados
{
    public static Gravador listaEstatica(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Valores da lista imutável");

        return anim;
    }

    public static Gravador pesquisaBinaria(List<Integer> valores, int chave){
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Valores da lista imutável");

        mergeSort(valores);

        int inicio = 0;
        int fim = valores.size() - 1;
        while (inicio <= fim) {
            int meio = (inicio + fim) / 2;
            if (chave == valores.get(meio)) {
                anim.gravarIndiceDestacado(valores,  meio, "Valor encontrado");
                return anim;
            } else if (chave > valores.get(meio)) {
                inicio = meio + 1;
                anim.gravarIndiceDestacado(valores,  inicio, "Pesquisa");
            } else {
                fim = meio - 1;
                anim.gravarIndiceDestacado(valores,  fim, "Pesquisa");
            }
        }

        anim.gravarLista(valores, "Disposição Final");

        return anim;
    }

    public static Gravador pesquisaSequencial(List<Integer> valores, int chave) {
        /* Cria o objeto. */
        Gravador anim = new Gravador();

        /* Grava a lista numa sequência, contendo seus valores, as cores e também o nome da mesma. */
        anim.gravarLista(valores, "Inicio de pesquisa sequencial");
        
        int i = 0;

        /* Também grava a lista na sequência, porém definindo a cor amarela para o indice informado */
        anim.gravarIndiceDestacado(valores, i, "Pesquisa sequencial");

        /* Enquanto o índice for menor que o tamanho da lista e valor do mesmo for diferente da chave
         * soma 1 no índice e grava a lista definindo a cor amarela para o índice informado.
         */
        while (i < valores.size() && valores.get(i) != chave) {
            i++;
            anim.gravarIndiceDestacado(valores, i, "Pesquisa sequencial");
        }

        /* Se o i for menor que o tamanho da lista, grava a lista definindo a cor amarela para o índice informado.  */
        if (i < valores.size()) {
            anim.gravarIndiceDestacado(valores, i, "Chave encontrada");
        }
        /* Caso contrário, grava a lista sem definir a cor  */
        else {
            anim.gravarLista(valores, "Chave não encontrada");
        }

        return anim;
    }

    public static Gravador classificarPorBolha(List<Integer> valores) {
        Gravador anim = new Gravador();

        anim.gravarLista(valores, "Disposição inicial");

        boolean houvePermuta;
        do {
            houvePermuta = false;

            for (int i = 1; i < valores.size(); i++) {
                anim.gravarComparacaoSimples(valores, i-1, i);
                if (valores.get(i-1) > valores.get(i)) {
                    permutar(valores, i - 1, i);
                    anim.gravarPosTrocas(valores, i-1, i);
                    houvePermuta = true;
                }
            }
        } while (houvePermuta);

        anim.gravarLista(valores, "Disposição final");

        return anim;
    }

    public static Gravador classificarPorSelecao(List<Integer> valores) {
        Gravador anim = new Gravador();

        anim.gravarLista(valores, "Disposição inicial");

        for (int i = 0; i < valores.size(); i++) {
            int menorIdx = encontrarIndiceMenorElem(valores, i);
            permutar(valores, menorIdx, i);
            anim.gravarPosTrocas(valores, menorIdx, i);
        }

        anim.gravarLista(valores, "Disposição final");

        return anim;
    }

    public static Gravador classificarPorInsercao(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");

        for (int i = 1; i < valores.size(); i++) {
            Integer elem = valores.get(i);

            int j = i;
            anim.gravarComparacaoSimples(valores, j, i);

            while (j > 0 && valores.get(j-1) > elem) {
                valores.set(j, valores.get(j-1)); anim.gravarPosTrocas(valores, j, j-1);
                j--;
                anim.gravarComparacaoSimples(valores, j, i);
            }
            valores.set(j, elem);
        }

        anim.gravarLista(valores, "Disposição final");
        return anim;
    }

    public static Gravador classificarPorMerge(List <Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");
        mergeSort(valores);
        anim.gravarLista(valores, "Disposição final");
        return anim;
    }

    public static Gravador classificarPorQuick(List <Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");
        quickSort(valores);
        anim.gravarLista(valores, "Disposição final");
        return anim;
    }

    public static void quickSort(List<Integer> valores) {
        Gravador anim = new Gravador();
        if (valores.isEmpty())
            return;
        else {
            Integer pivot = valores.get(0);

            List<Integer> less = new LinkedList<Integer>();
            List<Integer> pivotList = new LinkedList<Integer>();
            List<Integer> more = new LinkedList<Integer>();

            // Partition
            for (Integer i: valores) {
                if (i.compareTo(pivot) < 0)
                    less.add(i);
                else if (i.compareTo(pivot) > 0)
                    more.add(i);
                else
                    pivotList.add(i);
            }

            quickSort(less);
            quickSort(more);

            less.addAll(pivotList);
            less.addAll(more);
        }
    }

    private static void mergeSort(List<Integer> valores){
        Gravador anim = new Gravador();
        if (valores.size() > 1) {
            int idxMeio = 0;
            ArrayList<Integer> left = new ArrayList<>();
            ArrayList<Integer> right = new ArrayList<>();

            idxMeio = valores.size() / 2;
            for (int i = 0; i < idxMeio; i++)
                left.add(valores.get(i));

            for (int j = idxMeio; j < valores.size(); j++)
                right.add(valores.get(j));

            mergeSort(left);
            mergeSort(right);

            merge(valores, left, right);
        }
    }

    private static void merge(List<Integer> valores, List<Integer> left, List<Integer> right){
        Gravador anim = new Gravador();

        List<Integer> temp = null;
        int numbersIndex = 0;
        int leftIndex = 0;
        int rightIndex = 0;

        while (leftIndex < left.size() && rightIndex < right.size()) {
            anim.gravarComparacaoMerge(valores, left.get(leftIndex), right.get(rightIndex));
            if (left.get(leftIndex) < right.get(rightIndex) ) {
                valores.set(numbersIndex, left.get(leftIndex));
                leftIndex++;
            } else {
                valores.set(numbersIndex, right.get(rightIndex));
                rightIndex++;
            }
            numbersIndex++;
        }

        int tempIndex = 0;
        if (leftIndex >= left.size()) {
            temp = right;
            tempIndex = rightIndex;
        }
        else {
            temp = left;
            tempIndex = leftIndex;
        }

        for (int i = tempIndex; i < temp.size(); i++) {
            valores.set(numbersIndex, temp.get(i));
            numbersIndex++;
        }
    }

    private static int encontrarIndiceMenorElem(List<Integer> valores, int idxInicio) {
        Gravador anim = new Gravador();

        int menor = idxInicio;
        for (int i = idxInicio+1; i < valores.size(); i++) {
            anim.gravarComparacaoSimples(valores, menor, i);
            if (valores.get(menor) > valores.get(i))
                menor = i;
        }
        return menor;
    }

    private static void permutar(List<Integer> lista, int a, int b) {
        Integer permutador = lista.get(a); // permutador = lista[a]
        lista.set(a, lista.get(b)); // lista[a] = lista[b]
        lista.set(b, permutador); // lista[b] = permutador
    }
}